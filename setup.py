

from distutils.core import setup


setup(
    name="neohubby",
    version="0.0.4",
    author="Ali Afshar",
    author_email="aafshar@gmail.com",
    description="Python API for Neohub heating controllers from Heatmiser.",
    long_description="Python API for Neohub heating controllers from Heatmiser.",
    url="https://gitlab.com/afshar-oss/neohubby",
    py_modules=['neohubby'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    license='MIT',
)
